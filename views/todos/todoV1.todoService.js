angular.module('mod.m71')
    .factory('TodoService', ['TodoFactory', '$q', function(TodoFactory, $q) {
        var todoServ = {};

        todoServ.getConfig = function(tenantId, moduleId) {
            var deferred = $q.defer();
            TodoFactory.config().get({ 'tenantId': tenantId, 'moduleId': moduleId }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        todoServ.getMyTodos = function() {
            var deferred = $q.defer();
            TodoFactory.todos().get({ 'method': 'mine' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        todoServ.getActiveTodos = function() {
            var deferred = $q.defer();
            TodoFactory.todos().get({ 'method': 'active' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };


        todoServ.saveTodo = function(todo) {
            var deferred = $q.defer();
            TodoFactory.todos().save({ 'method': 'save' }, todo, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        todoServ.updateTodo = function(id, todo) {
            var deferred = $q.defer();
            TodoFactory.todos().update({ 'method': 'update', 'id': id }, todo, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        todoServ.deleteTodo = function(id) {
            var deferred = $q.defer();
            TodoFactory.todos().delete({ 'method': 'delete', 'id': id }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        todoServ.getCompletedTodos = function() {
            var deferred = $q.defer();
            TodoFactory.todos().get({ 'method': 'completed' },
                function(success) {
                    deferred.resolve(success);
                },
                function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        todoServ.getDeletedTodos = function() {
            var deferred = $q.defer();
            TodoFactory.todos().get({ 'method': 'archived' },
                function(success) {
                    deferred.resolve(success);
                },
                function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        todoServ.getLabels = function() {
            var deferred = $q.defer();
            TodoFactory.labels().get({ 'method': 'getLabels' },
                function(success) {
                    deferred.resolve(success);
                },
                function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        todoServ.createLabel = function(obj) {
            var deferred = $q.defer();
            TodoFactory.labels().save({ 'method': 'saveLabel' }, obj,
                function(success) {
                    deferred.resolve(success);
                },
                function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        todoServ.shareTodo = function(todoId, memberId) {
            var deferred = $q.defer();
            TodoFactory.share().get({ 'id': todoId, 'memberId': memberId },
                function(success) {
                    deferred.resolve(success);
                },
                function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };


        return todoServ;
    }])
    .factory('TodoFactory', ['$resource', 'MOD_todoV1', 'SessionService', function($resource, MOD_todoV1, SessionService) {

        var todoFact = {};
        todoFact.todos = function() {
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            return $resource(MOD_todoV1.API_URL + ':method/:id', {
                method: '@method',
                id: '@id'
            }, {
                'get': {
                    method: 'GET',
                    headers: {
                        Authorization: bearer
                    },
                    isArray: true
                },
                'save': {
                    method: 'POST',
                    headers: {
                        Authorization: bearer
                    }
                },
                'update': {
                    method: 'PUT',
                    headers: {
                        Authorization: bearer
                    }
                },
                'delete': {
                    method: 'PUT',
                    headers: {
                        Authorization: bearer
                    }
                },
                'list': {
                    method: 'GET',
                    headers: {
                        Authorization: bearer
                    }
                }
            })
        };

        todoFact.labels = function() {
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            return $resource(MOD_todoV1.API_URL + ':method/:id', {
                method: '@method',
                id: '@id'
            }, {
                'get': {
                    method: 'GET',
                    headers: {
                        Authorization: bearer
                    },
                    isArray: true
                },
                'save': {
                    method: 'POST',
                    headers: {
                        Authorization: bearer
                    }
                },
                'update': {
                    method: 'PUT',
                    headers: {
                        Authorization: bearer
                    }
                },
                'delete': {
                    method: 'PUT',
                    headers: {
                        Authorization: bearer
                    }
                }
            })
        };

        todoFact.config = function() {
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            return $resource(MOD_todoV1.CONFIG_URL + 'config/tenant/:tenantId/:moduleId', {
                tenantId: '@tenantId',
                moduleId: '@moduleId'
            }, {
                'get': {
                    method: 'GET',
                    headers: {
                        Authorization: bearer
                    },
                    isArray: true
                }
            })
        };

        todoFact.share = function() {
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            return $resource(MOD_todoV1.API_URL + 'share/:id/:memberId', {
                memberId: '@memberId',
                id: '@id'
            }, {
                'get': {
                    method: 'GET',
                    headers: {
                        Authorization: bearer
                    }
                }
            })
        };

        return todoFact;
    }]);
