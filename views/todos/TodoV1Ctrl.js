/**
 * mod.m71 Module
 *
 * Description
 */
angular.module('mod.m71').
controller('TodoV1Ctrl', ['$scope', 'TodoService', 'APP_CONSTANTS', '$rootScope', function($scope, TodoService, APP_CONSTANTS, $rootScope) {

    $rootScope.tenantId = APP_CONSTANTS.TENANT_ID;
    //Need to fetch from Server dont know how
    var moduleId = "MOD:d4a054ff-db51-4f48-8be8-8da8d4ca1270";
    $rootScope.todoConfig = {
        "share": false,
        "delete": false
    }
    var getConfig = function() {
        TodoService.getConfig($rootScope.tenantId, moduleId).then(function(response) {
            _.each(response, function(item) {
                if (item.name == 'isShare') {
                    if (item.values == 'true') {
                        $scope.todoConfig.share = true;
                    }
                }
            })
        }, function(error) {
            console.log("error retrieving module config");
        })
    };

    var init = function() {
        getConfig();
    };
    init();
}]);
