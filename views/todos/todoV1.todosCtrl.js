angular.module('mod.m71')
    .controller('todoCtrl', ['$scope', '$rootScope', 'TodoService', '_', '$mdDialog', 'AlertService', 'UserService', function($scope, $rs, TodoService, _, $mdDialog, AlertService, UserService) {

        $scope.configValues = $rs.todoConfig;
        $scope.currentNavItem = "all";
        $scope.isEmpty = false;
        $scope.isShare = false;
        $scope.user = {
            "dueDate": new Date(),
            "label": "inbox",
            "isCompleted": false,
            "isDelete": false
        };

        var getUsers = function() {
            $scope.users = [];
            UserService.getTenantUsers($rs.tenantId).then(function(response) {
                $scope.users = response;
            }, function(response) {
                console.log("error");
            })
        };

        var getMyTodos = function() {
            TodoService.getActiveTodos().then(function(response) {
                angular.forEach(response, function(value, key) {
                    value.dueDate = new Date(value.dueDate).setHours(0, 0, 0, 0);
                })
                $scope.todos = _.groupBy(response, "dueDate");
                if (Object.keys($scope.todos).length == 0) {
                    $scope.isEmpty = true;
                } else {
                    $scope.isEmpty = false;
                }

            }, function(error) {
                console.log("Error retreiving todos");
            });
        };

        var getCompletedTodos = function() {
            TodoService.getCompletedTodos().then(function(response) {
                angular.forEach(response, function(value, key) {
                    value.dueDate = new Date(value.dueDate).setHours(0, 0, 0, 0);
                })
                $scope.todos = _.groupBy(response, "dueDate");
                if (Object.keys($scope.todos).length == 0) {
                    $scope.isEmpty = true;
                } else {
                    $scope.isEmpty = false;
                }
            }, function(error) {
                console.log("Error retreiving todos");
            });
        };

        var getLabels = function() {
            TodoService.getLabels().then(function(response) {
                $scope.labels = response;
            }, function(error) {
                console.log("Error retreiving todos");
            });
        };

        $scope.addLabel = function(ev) {
            var labelExist = false;

            var confirm = $mdDialog.prompt()
                .title('What would you name your label?')
                .textContent('Inbox is a common name.')
                .placeholder('label')
                .ariaLabel('Label')
                .initialValue('')
                .targetEvent(ev)
                .ok('Okay!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function(result) {
                _.each($scope.labels, function(item) {
                    console.log(item.label);
                    if (item.label.toLowerCase() == result.toLowerCase()) {
                        labelExist = true;
                    }
                });
                if (labelExist) {
                    AlertService.alert("Label exists", 'md-warn');
                } else if (!result) {
                    AlertService.alert("Enter valid label name", 'md-warn');
                } else {
                    TodoService.createLabel({ 'label': result }).then(function(response) {
                        $scope.labels.push(response);
                    }, function(error) {
                        console.log("Error retreiving todos");
                    });
                }
            }, function() {
                $scope.status = 'You didn\'t name your dog.';
            });
        };

        $scope.addTodo = function() {
            if ($scope.user.name == "") {
                AlertService.alert("Enter valid text", 'md-warn');
            } else {
                TodoService.saveTodo($scope.user).then(function(response) {
                    $scope.user.name = "";
                    getMyTodos();
                    AlertService.alert("Todo added Successfully", 'md-primary');
                }, function(error) {
                    console.log("Error adding todos");
                });
            }
        };

        $scope.editTodo = function(ev, todo) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, todo, labels, TodoService) {
                        $scope.labels = labels;
                        $scope.todo = todo;
                        $scope.todo.dueDate = new Date($scope.todo.dueDate);
                        console.log(todo.dueDate);
                        $scope.create = function() {
                            TodoService.updateTodo(todo._id, $scope.todo).then(function(response) {
                                AlertService.alert("Todo updated Successfully", 'md-primary');
                                $mdDialog.hide();
                            }, function(error) {
                                console.log("Error updating todos");
                            });
                        };

                        $scope.closeDialog = function() {
                            $mdDialog.cancel();
                        };
                    },
                    templateUrl: 'mod_todoV1/views/todos/EditTodo.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    locals: {
                        "todo": todo,
                        "labels": $scope.labels
                    }
                })
                .then(function() {
                    if ($scope.currentNavItem == 'all') {
                        getMyTodos();
                    } else {
                        getCompletedTodos();
                    }
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        $scope.updateTodo = function(todo) {
            TodoService.updateTodo(todo._id, todo).then(function(response) {
                if ($scope.currentNavItem == 'all') {
                    getMyTodos();
                } else {
                    getCompletedTodos();
                }
                AlertService.alert("Todo updated Successfully", 'md-primary');
            }, function(error) {
                console.log("Error updating todos");
            });
        };

        $scope.share = function(ev, todo) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, todo, users, TodoService, _) {
                        $scope.users = users;
                        _.each($scope.users, function(user) {
                            user.isChecked = false;
                        });
                        $scope.todo = todo;

                        $scope.create = function() {
                            _.each($scope.users, function(user) {
                                if (user.isChecked) {
                                    TodoService.shareTodo(todo._id, user.uuid).then(function(response) {
                                        if (response.error) {
                                            AlertService.alert(response.error, 'md-warn');
                                        } else {
                                            AlertService.alert("Todo Shared Successfully", 'md-primary');
                                            $mdDialog.hide();
                                        }

                                    }, function(error) {
                                        console.log("Error updating todos");
                                        console.log(error);
                                        
                                    });
                                };
                            });

                        };

                        $scope.closeDialog = function() {
                            $mdDialog.cancel();
                        };
                    },
                    templateUrl: 'mod_todoV1/views/todos/shareTodo.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    locals: {
                        "todo": todo,
                        "users": $scope.users

                    }
                })
                .then(function() {

                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        }

        $scope.goto = function(page) {
            if (page == 'all') {
                getMyTodos();
            } else if (page == 'completed') {
                getCompletedTodos();
            }
        }

        var init = function() {
            getMyTodos();
            getLabels();
            getUsers();
        };

        init();
    }]);
