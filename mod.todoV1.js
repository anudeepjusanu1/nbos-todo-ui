angular.module('mod.m71', []);

angular.module('mod.m71')
  .constant('MOD_todoV1', {
        API_URL: 'https://sheltered-oasis-41359.herokuapp.com/api/todos/',
        API_URL_DEV: '',
        CONFIG_URL: 'http://api.qa1.nbos.in/api/core/v0/'
    })
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m71', {
                url: '/todoV1',
                parent: 'layout',
                template: "<div ui-view></div>",
                controller: "TodoV1Ctrl",
                data: {
                    type: 'home'
                }
            })
            .state('m71.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_todoV1/views/menu/todoV1.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "todoV1"
               }
           })
            .state('m71.todos', {
                url: '/todos',
                templateUrl: "mod_todoV1/views/todos/todoV1.todos.html",
                controller: "todoCtrl",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Todos",
                    module: "todoV1"
                }

            })

    }])